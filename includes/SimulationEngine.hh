#pragma once

#include "Parser.hpp"

class SimulationEngine {
  public:
      static std::string const simulate(std::string const &filePath);
      ~SimulationEngine() = default;

  private:    
      SimulationEngine();
      SimulationEngine(SimulationEngine const &) = delete;

      void startSimulation(std::string const &filePath) ;
      void simulationLoop();

      std::string const getResult() const;
      void logResultInFile(std::string const &path) const;
      void logObject(std::stringstream &log) const;
      std::string const logResult() const;

      std::size_t getMaxIterationNeeded() const;

      Map<Object>                   map;
      std::vector<Adventurer>       adventurers {};
};