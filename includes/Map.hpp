#pragma once

#include <vector>
#include <iostream>
#include <exception>

#define RESERVECAPACITY 128

template <typename T>
class Map {
      public:
            Map() : map{} {
                  this->map.reserve(this->currentCapacity);
                  this->currentCapacity = this->map.capacity();
            }

            Map(std::size_t const mapWidth, std::size_t const mapHeight, T const &fillAllWith) : map{} {
                  this->map.reserve(this->currentCapacity);
                  this->currentCapacity = this->map.capacity();
                  fillMap(mapWidth, mapHeight, fillAllWith);
                  this->width = mapWidth;
                  this->height = mapHeight;
                  if (this->map.empty()) {
                        throw std::length_error("Error: Map has a (0,0) size");
                  }
            }

            Map(Map const &other) {
                  this->width = other.width;
                  this->height = other.height;
                  this->map = other.map;
                  this->currentCapacity = other.currentCapacity;
            }

            Map &operator=(Map const &other) {
                  this->width = other.width;
                  this->height = other.height;
                  this->map = other.map;
                  this->currentCapacity = other.currentCapacity;
                  return *this;
            }

            ~Map() = default;

            T &getElement(std::size_t const x, std::size_t const y) {
                  std::size_t const it = y * this->width + x;

                  if (it >= this->map.size()) {
                        throw std::out_of_range("Out of range access");
                  }
                  return this->map[it];
            }
            
            T const &getElement(std::size_t const x, std::size_t const y) const {
                  std::size_t const it = y * this->width + x;

                  if (it >= this->map.size()) {
                        throw std::out_of_range("Out of range access");
                  }
                  return this->map[it];
            }

            T const *operator[](std::size_t const index) const {
                  return &this->map[index * this->width];
            }

            T *operator[](std::size_t const index) {
                  return &this->map[index * this->width];
            }

            std::size_t getSize() const { return (this->width * this->height); }

            std::size_t getWidth() const { return (this->width); }

            std::size_t getHeight() const { return (this->height); }

            void  resizeTo(std::size_t x, std::size_t y, T const &fillAllWith, bool isFinal = false) {
                  if (x + 1 < this->width || y < this->height) {
                        finalSize(x, y, fillAllWith, isFinal);
                        return ;
                  }
                  if (x * y > this->currentCapacity) {
                        reserveSpaceForMap(std::size_t((x * y) / RESERVECAPACITY));
                  }
                  if (this->map.size() <= 0) {
                        if (!isFinal) {
                              ++x;
                              ++y;
                        }
                        fillMap(x, y, fillAllWith);
                        setSize(x, y);
                        return ;
                  }
                  extend(x, y + 1, fillAllWith);
                  setSize(x + 1, y + 1);
                  if (isFinal) {
                        shrink(x, y);
                        setSize(x, y);  
                  }
            }

            void print() const {
                  for (std::size_t index = 0; index < this->height * this->width; index++) {
                        if (index != 0 && index % this->width == 0) {
                              std::cout << "|" << std::endl;
                        }
                        std::cout << this->map[index].abbreviation << " ";
                  }
                  std::cout << "|" << std::endl << std::endl;
            }

      private:
            void fillMap(std::size_t const newWidth, std::size_t const newHeight, T const &fillAllWith) {
                  for (std::size_t index = 0; index < newWidth * newHeight; index++) {
                        this->map.push_back(fillAllWith);
                  }
            }

            void  reserveSpaceForMap(size_t const numberOfTime) {
                  std::size_t newCapacity = (numberOfTime * std::size_t(RESERVECAPACITY)) + this->currentCapacity;
                  
                  this->map.reserve(newCapacity);
                  if (this->map.capacity() < newCapacity) {
                        throw std::runtime_error("Error: Not enough memory");
                  }
                  this->currentCapacity = newCapacity;
            }

            void extend(std::size_t const x, std::size_t const y, T const &fillAllWith) {
                  std::size_t size = (x + 1) * y;
                  std::size_t sizeBeforeResize = this->map.size();
                  std::size_t offset = (x + 1) - this->width;
                  std::size_t counter = 0;
                  
                  this->map.resize(size, fillAllWith);
                  auto it = --this->map.end();
                  it = it - static_cast<int>(((x + 1) * (y - this->height) + offset));

                  while (sizeBeforeResize > 1) {
                        --sizeBeforeResize;
                        *it = this->map[sizeBeforeResize];
                        ++counter;
                        it--;
                        if (counter > this->width - 1) {
                              counter = 0;
                              while (counter < offset) {
                                    *it = fillAllWith;
                                    ++counter;
                                    it--;
                              }
                              counter = 0;
                        }
                  }
            }

            void shrink(std::size_t const x, std::size_t const y) {
                  std::size_t size = (x + 1) * y;
                  std::size_t offset = this->width - x;
                  std::size_t counter = 1, idx = 0;

                  this->width > x ? offset = this->width - x : offset = x - this->width;
                  auto it = std::next(this->map.begin(), static_cast<int>(x));
                  while (counter < y) {
                        ++idx;
                        *it = *(it + static_cast<int>(counter * offset));
                        if (idx >= x) {
                              idx = 0;
                              ++counter;
                        }
                        ++it;
                  }
                  this->map.resize(size);
            }

            void finalSize(std::size_t x, std::size_t y, T const &fillAllWith, bool isFinal) {
                  if (x == 0 && y == 0) {
                        return;
                  }
                  x == 0 ? x = 1 : x;
                  y == 0 ? y = 1 : y;
                  if (isFinal && (x + 1 < this->width && y < this->height)) {
                        shrink(x, y);
                        setSize(x, y);
                  }
                  else if (isFinal) {
                        std::size_t tmpX = x, tmpY = y;
                        tmpX > this->width ? tmpX : tmpX = this->width;
                        tmpY > this->height ? tmpY : tmpY = this->height;
                        extend(tmpX, tmpY + 1, fillAllWith);
                        setSize(tmpX + 1, tmpY + 1);
                        shrink(x, y);
                        setSize(x, y);
                  }
            }

            void setSize(std::size_t const newWidth, std::size_t const newHeight) {
                  this->width = newWidth;
                  this->height = newHeight;
            }

            std::vector<T>    map {};
            std::size_t       width {};
            std::size_t       height {};
            std::size_t       currentCapacity {};
};