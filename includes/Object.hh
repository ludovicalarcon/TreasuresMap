#pragma once

#include <cstddef>

struct      Object {
      enum Type {
            Mountain,
            Treasure,
            Adventurer,
            Map,
            Comment,
            Lowland,
            Undefined
      };

      Object(Type const objType, char const objAbbreviation, std::size_t const objCount = 1);
      Object(Object const &);
      Object() = default;
      ~Object() = default;

      Type        type {Undefined};
      char        abbreviation {};
      std::size_t count {};
      bool        isOccupied {false};
};