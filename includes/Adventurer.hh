#pragma once

#include <string>
#include <vector>

#include "Map.hpp"
#include "Object.hh"

enum Orientation {
      North = 0,
      East,
      South,
      West,
      Undefined
};

enum Movement {
      Forward,
      Right,
      Left,
      Unknown
};

class Adventurer {
      public:
      Adventurer() = delete;
      Adventurer(std::string const &advName, std::size_t advX,
                  std::size_t advY, Orientation const advOrientation,
                  std::string const &moves);
      Adventurer(Adventurer const &other);
      ~Adventurer() = default;

      void move(Map<Object> &map);

      std::string const &getName() const;
      std::size_t getXCoordinate() const;
      std::size_t getYCoordinate() const;
      std::size_t getNumberOfMovements() const;
      std::size_t getNumberOfTreasures() const;
      char getOrientationAbbreviation() const;

      private:
            Movement getMovementToken(char const abbreviation);
            void moveForward(Map<Object> &map);
            void moveRight();
            void moveLeft();

            bool moveToNorth(Map<Object> &map);
            bool moveToEast(Map<Object> &map);
            bool moveToSouth(Map<Object> &map);
            bool moveToWest(Map<Object> &map);


            std::string       name;

            std::size_t       coordX {};
            std::size_t       coordY {};
            Orientation       currentOrientation;

            std::size_t       numberOfTreasures {};

            std::vector<Movement>   movements;
            std::size_t             currentMovement {};
};