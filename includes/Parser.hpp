#pragma once

#include <string>
#include <iostream>
#include <array>
#include <sstream>
#include <fstream>

#include "Map.hpp"
#include "Object.hh"
#include "Adventurer.hh"

class Parser {
      public:
            Map<Object> const &parseFromFile();
            Parser(std::string const &filePath);
            Parser(const Parser &) = delete;
            Parser() = delete;
            ~Parser() = default;

            std::vector<Adventurer> const &getAdventurersList() const;
      private:
            Map<Object> const &computeMap();

            Object::Type getTokenType(char const) const;
            Orientation getOrientationByToken(char const) const;

            template<typename T>
            T     extractDataFromString(std::string const &line, std::size_t const start, std::size_t const end) {
                  std::stringstream ss {};
                  std::string stringData {};
                  T data {};

                  stringData = line.substr(start, end);
                  ss.str(stringData);
                  ss >> data;
                  return data;
            }

            bool checkForResizingMap(std::size_t x, std::size_t y, Object const & obj);
            bool updateInternalFields(Object const &obj, std::size_t x, std::size_t y, bool isMountain = false);

            bool  addMountainOnMap(std::string const &line);
            bool  addTreasureOnMap(std::string const &line);
            bool  addAdventurerOnMap(std::string const &line);
            bool  resizeMapWithFinalSize(std::string const &line);
            bool  addComment(std::string const &line);
            bool  unknownCommand(std::string const &line);

            using LineParser = bool (Parser::*)(std::string const &);

            std::size_t       lineNumber {1};
            std::ifstream     file;

            Map<Object>                   map;
            std::array<LineParser, 7>     parsingMethods {};
            bool                          foundMapSize {false};
            std::size_t                   mapXCoord {0};
            std::size_t                   mapYCoord {0};

            std::vector<Adventurer>       adventurers {};
};