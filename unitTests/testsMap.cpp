#include "Map.hpp"
#include "Object.hh"
#include "Catch.hpp"

TEST_CASE("Map size", "[MAP]") {
      Object obj(Object::Type::Lowland, '.');
      Map<Object> map(std::size_t(5), std::size_t(4), obj);
      
      REQUIRE(map.getSize() == 20);
      map = Map<Object>(std::size_t(40), std::size_t(60), obj);
      REQUIRE(map.getSize() == 2400);
      REQUIRE_THROWS_AS((Map<Object>(std::size_t(0), std::size_t(0), obj)), std::length_error);
}

TEST_CASE("Map resize", "[MAP]") {
      Map<Object> map;
      Object obj(Object::Type::Lowland, '.');
      Object mountain(Object::Type::Lowland, 'M');

      map.resizeTo(3, 4, obj);
      map[0][0] = mountain;
      map[1][1] = mountain;
      map[2][2] = mountain;
      REQUIRE(map.getSize() == 20);
      SECTION("Resizing bigger") {
            map.resizeTo(10, 32, obj);
            REQUIRE(map.getSize() == 363);
            REQUIRE(map.getElement(0, 0).abbreviation == 'M');
            REQUIRE(map.getElement(1, 1).abbreviation == 'M');
            REQUIRE(map.getElement(2, 2).abbreviation == 'M');
            map.print();
      }
      SECTION("Resizing bigger in X but same in Y") {
            map.resizeTo(5, 4, obj);
            REQUIRE(map.getSize() == 20);
            REQUIRE(map.getElement(0, 0).abbreviation == 'M');
            REQUIRE(map.getElement(1, 1).abbreviation == 'M');
            REQUIRE(map.getElement(2, 2).abbreviation == 'M');
            map.print();
      }
      SECTION("Resizing bigger in Y but same in X") {
            map.resizeTo(3, 5, obj);
            REQUIRE(map.getSize() == 24);
            REQUIRE(map.getElement(0, 0).abbreviation == 'M');
            REQUIRE(map.getElement(1, 1).abbreviation == 'M');
            REQUIRE(map.getElement(2, 2).abbreviation == 'M');
            map.print();
      }
      SECTION("Resizing bigger in Y but smaller in X => must make no change") {
            map.resizeTo(2, 6, obj);
            REQUIRE(map.getSize() == 20);
            REQUIRE(map.getElement(0, 0).abbreviation == 'M');
            REQUIRE(map.getElement(1, 1).abbreviation == 'M');
            REQUIRE(map.getElement(2, 2).abbreviation == 'M');
      }
      SECTION("Resizing bigger in X but smaller in Y => must make no change") {
            map.resizeTo(5, 3, obj);
            REQUIRE(map.getSize() == 20);
            REQUIRE(map.getElement(0, 0).abbreviation == 'M');
            REQUIRE(map.getElement(1, 1).abbreviation == 'M');
            REQUIRE(map.getElement(2, 2).abbreviation == 'M');
      }
      SECTION("Resizing by 0 => must make no change") {
            map.resizeTo(0, 0, obj);
            REQUIRE(map.getSize() == 20);
      }
      SECTION("Resizing by 1") {
            map.resizeTo(1, 1, obj, true);
            REQUIRE(map.getSize() == 1);
            REQUIRE(map.getElement(0, 0).abbreviation == 'M');
            map.print();
      }
      SECTION("Resizing bigger and then smaller") {
            map.resizeTo(10, 32, obj);
            REQUIRE(map.getSize() == 363);
            map.resizeTo(3, 4, obj, true);
            REQUIRE(map.getSize() == 12);
            REQUIRE(map.getElement(0, 0).abbreviation == 'M');
            REQUIRE(map.getElement(1, 1).abbreviation == 'M');
            REQUIRE(map.getElement(2, 2).abbreviation == 'M');
            map.print();
      }
}

TEST_CASE("Map operator[] && getElement", "[MAP]") {
      Object obj(Object::Type::Lowland, '.');
      Map<Object> map(std::size_t(8), std::size_t(4), obj);
      
      map[0][0] = Object(Object::Type::Mountain, 'M');
      map[3][0] = Object(Object::Type::Mountain, 'M');
      map[2][3] = Object(Object::Type::Mountain, 'M');
      map[3][3] = Object(Object::Type::Mountain, 'A');
      map[3][7] = Object(Object::Type::Mountain, 'T');
      map.resizeTo(10, 6, obj);
      REQUIRE(map.getElement(0, 0).abbreviation == 'M');
      REQUIRE(map.getElement(0, 3).abbreviation == 'M');
      REQUIRE(map.getElement(3, 2).abbreviation == 'M');
      REQUIRE(map.getElement(3, 3).abbreviation == 'A');
      REQUIRE(map.getElement(7, 3).abbreviation == 'T');
      REQUIRE_THROWS_AS(map.getElement(11, 6), std::out_of_range);
}
