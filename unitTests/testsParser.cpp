#include "Parser.hpp"
#include "Catch.hpp"

SCENARIO("Parsing and creating map", "[PARSER]" ) {
      GIVEN("An empty map which need to be construct by parsing map file") {
            Map<Object> map;
            REQUIRE(map.getSize() == 0);

            WHEN("Parsing map1.txt") {
                  Parser parser("../../maps/map1.txt");
                  map = parser.parseFromFile();
                  REQUIRE(map.getElement(1, 1).type == Object::Mountain);
                  REQUIRE(map.getElement(3, 0).type == Object::Treasure);
                  REQUIRE(map.getElement(3, 0).count == 2);
                  REQUIRE(map.getElement(5, 0).type == Object::Mountain);
                  REQUIRE(map.getElement(2, 3).type == Object::Mountain);
                  REQUIRE(map.getElement(1, 2).type == Object::Adventurer);
                  REQUIRE(parser.getAdventurersList()[0].getName() == "Lara");
                  REQUIRE(parser.getAdventurersList()[0].getOrientationAbbreviation() == 'S');
            }
            WHEN("Parsing map2.txt") {
                  Parser parser("../../maps/map2.txt");
                  map = parser.parseFromFile();
                  REQUIRE(map.getElement(1, 0).type == Object::Mountain);
                  REQUIRE(map.getElement(2, 1).type == Object::Mountain);
                  REQUIRE(map.getElement(0, 3).type == Object::Treasure);
                  REQUIRE(map.getElement(0, 3).count == 2);
                  REQUIRE(map.getElement(1, 3).type == Object::Treasure);
                  REQUIRE(map.getElement(1, 3).count == 3);
                  REQUIRE(map.getElement(1, 2).type == Object::Adventurer);
                  REQUIRE(parser.getAdventurersList()[0].getName() == "Lara");
                  REQUIRE(parser.getAdventurersList()[0].getOrientationAbbreviation() == 'S');
            }
            WHEN("Parsing map3.txt") {
                  Parser parser("../../maps/map3.txt");
                  map = parser.parseFromFile();
                  REQUIRE(map.getElement(0,0).type == Object::Adventurer);  
                  REQUIRE(parser.getAdventurersList()[0].getName() == "Bob");
                  REQUIRE(parser.getAdventurersList()[0].getOrientationAbbreviation() == 'E');
                  REQUIRE(map.getElement(1,0).type == Object::Mountain);  
                  REQUIRE(map.getElement(1,1).type == Object::Mountain);  
                  REQUIRE(map.getElement(0,1).type == Object::Treasure);              
                  REQUIRE(map.getElement(0, 1).count == 4);
            }
            WHEN("Parsing map4.txt") {
                  Parser parser("../../maps/map4.txt");
                  map = parser.parseFromFile();
                  REQUIRE(map.getElement(0,0).type == Object::Mountain);
                  REQUIRE(map.getElement(0,1).type == Object::Mountain);
                  REQUIRE(map.getElement(0,2).type == Object::Mountain);
                  REQUIRE(map.getElement(1,0).type == Object::Adventurer);  
                  REQUIRE(parser.getAdventurersList()[0].getName() == "Lara");
                  REQUIRE(parser.getAdventurersList()[0].getOrientationAbbreviation() == 'S');
                  REQUIRE(map.getElement(1,1).type == Object::Treasure);              
                  REQUIRE(map.getElement(1, 1).count == 1);
                  REQUIRE(map.getElement(1,2).type == Object::Adventurer);  
                  REQUIRE(parser.getAdventurersList()[1].getName() == "Bob");
                  REQUIRE(parser.getAdventurersList()[1].getOrientationAbbreviation() == 'N');
                  REQUIRE(map.getElement(1,0).type == Object::Adventurer);  
                  REQUIRE(map.getElement(2,0).type == Object::Mountain);
                  REQUIRE(map.getElement(2,1).type == Object::Treasure);              
                  REQUIRE(map.getElement(2, 1).count == 1);
                  REQUIRE(map.getElement(2,2).type == Object::Mountain);
                  REQUIRE(map.getElement(3,0).type == Object::Mountain);
                  REQUIRE(map.getElement(3,1).type == Object::Treasure);              
                  REQUIRE(map.getElement(3, 1).count == 1);
                  REQUIRE(map.getElement(3,2).type == Object::Mountain);
                  REQUIRE(map.getElement(4,0).type == Object::Adventurer);  
                  REQUIRE(parser.getAdventurersList()[2].getName() == "Tom");
                  REQUIRE(parser.getAdventurersList()[2].getOrientationAbbreviation() == 'S');
                  REQUIRE(map.getElement(4,1).type == Object::Treasure);              
                  REQUIRE(map.getElement(4, 1).count == 1);
                  REQUIRE(map.getElement(4,2).type == Object::Adventurer);  
                  REQUIRE(parser.getAdventurersList()[3].getName() == "Bruce");
                  REQUIRE(parser.getAdventurersList()[3].getOrientationAbbreviation() == 'N');
                  REQUIRE(map.getElement(5,0).type == Object::Mountain);
                  REQUIRE(map.getElement(5,1).type == Object::Mountain);
                  REQUIRE(map.getElement(5,2).type == Object::Mountain);
            }
            WHEN("Parsing map5.txt") {
                  Parser parser("../../maps/map5.txt");
                  map = parser.parseFromFile();
                  REQUIRE(map.getElement(0,0).type == Object::Mountain);
                  REQUIRE(map.getElement(0,1).type == Object::Mountain);
                  REQUIRE(map.getElement(0,2).type == Object::Mountain);
                  REQUIRE(map.getElement(0,3).type == Object::Mountain);
                  REQUIRE(map.getElement(0,4).type == Object::Mountain);
                  REQUIRE(map.getElement(1,0).type == Object::Mountain);
                  REQUIRE(map.getElement(1,1).type == Object::Mountain);
                  REQUIRE(map.getElement(1,2).type == Object::Treasure);              
                  REQUIRE(map.getElement(1, 2).count == 1);
                  REQUIRE(map.getElement(1,3).type == Object::Mountain);
                  REQUIRE(map.getElement(1,4).type == Object::Mountain);
                  REQUIRE(map.getElement(2,0).type == Object::Mountain);
                  REQUIRE(map.getElement(2,1).type == Object::Mountain);
                  REQUIRE(map.getElement(2,2).type == Object::Treasure);              
                  REQUIRE(map.getElement(2, 2).count == 1);
                  REQUIRE(map.getElement(2,3).type == Object::Mountain);
                  REQUIRE(map.getElement(2,4).type == Object::Mountain);
                  REQUIRE(map.getElement(3,0).type == Object::Mountain);
                  REQUIRE(map.getElement(3,1).type == Object::Mountain);                  
                  REQUIRE(map.getElement(3,2).type == Object::Treasure);              
                  REQUIRE(map.getElement(3, 2).count == 1);
                  REQUIRE(map.getElement(3,3).type == Object::Mountain);
                  REQUIRE(map.getElement(3,4).type == Object::Mountain);
                  REQUIRE(map.getElement(4,0).type == Object::Mountain);
                  REQUIRE(map.getElement(4,4).type == Object::Mountain);
                  REQUIRE(map.getElement(5,0).type == Object::Mountain);
                  REQUIRE(map.getElement(5,2).type == Object::Mountain);
                  REQUIRE(map.getElement(5,4).type == Object::Mountain);
                  REQUIRE(map.getElement(6,0).type == Object::Mountain);
                  REQUIRE(map.getElement(6,2).type == Object::Mountain);
                  REQUIRE(map.getElement(6,4).type == Object::Mountain);
                  REQUIRE(map.getElement(7,0).type == Object::Mountain);
                  REQUIRE(map.getElement(7,2).type == Object::Mountain);
                  REQUIRE(map.getElement(7,4).type == Object::Mountain);
                  REQUIRE(map.getElement(8,1).type == Object::Adventurer);  
                  REQUIRE(parser.getAdventurersList()[0].getName() == "Lara");
                  REQUIRE(parser.getAdventurersList()[0].getOrientationAbbreviation() == 'O');
                  REQUIRE(map.getElement(8,3).type == Object::Adventurer);  
                  REQUIRE(parser.getAdventurersList()[1].getName() == "Bob");
                  REQUIRE(parser.getAdventurersList()[1].getOrientationAbbreviation() == 'O');
            }
            WHEN("Parsing invalid.txt") {
                  Parser parser("../../maps/invalid.txt");
                  REQUIRE_THROWS_AS(parser.parseFromFile(), std::runtime_error);

            }
            WHEN("Parsing toto.txt") {
                  REQUIRE_THROWS_AS(Parser("../../maps/toto.txt"), std::runtime_error);
            }
            WHEN("Parsing mapWithNoLectureRight") {
                  REQUIRE_THROWS_AS(Parser("../../maps/mapWithNoLectureRight.txt"), std::runtime_error);
            }
      }
}