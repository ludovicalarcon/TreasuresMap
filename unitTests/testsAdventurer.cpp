#include "Adventurer.hh"
#include "Object.hh"
#include "Catch.hpp"

TEST_CASE("Adventurer checks field", "[ADVENTURER]") {
      Adventurer john("John", 2, 21, Orientation::North, "ADAAAGA");
      Adventurer lara("Lara", 30, 5, Orientation::South, "DAAGAADA");
      Adventurer croft("CROFT", 1, 1, Orientation::East, "ADA");

      REQUIRE(john.getName() == "John");
      REQUIRE(john.getXCoordinate() == 2);
      REQUIRE(john.getYCoordinate() == 21);
      REQUIRE(john.getNumberOfMovements() == 7);

      REQUIRE(lara.getName() == "Lara");
      REQUIRE(croft.getName() == "CROFT");
      REQUIRE(lara.getXCoordinate() == 30);
      REQUIRE(lara.getYCoordinate() == 5);
      REQUIRE(lara.getNumberOfMovements() == 8);
      REQUIRE(croft.getXCoordinate() == 1);
      REQUIRE(croft.getYCoordinate() == 1);
      REQUIRE(croft.getNumberOfMovements() == 3);
}

TEST_CASE("Adventurer moves 1x1 map", "[ADVENTURER]") {
      Map<Object> map(1, 1, Object(Object::Type::Lowland, '.'));
      Adventurer lara("Lara", 0, 0, Orientation::South, "DAAGAADA");
      lara.move(map);
      lara.move(map);
      lara.move(map);
      lara.move(map);
      REQUIRE(lara.getXCoordinate() == 0);
      REQUIRE(lara.getYCoordinate() == 0);    
}

TEST_CASE("Adventurer in (0,0) with West Orientation moves 2x2 map : AADAADAA", "[ADVENTURER]") {
      std::string moves = "AADAADAA";
      Map<Object> map(2, 2, Object(Object::Type::Lowland, '.'));
      Adventurer lara("Lara", 0, 0, Orientation::West, moves);
      for (std::size_t i = 0; i < moves.size(); i++) {
            lara.move(map);
      }
      REQUIRE(lara.getXCoordinate() == 1);
      REQUIRE(lara.getYCoordinate() == 0);    
}

TEST_CASE("Adventurer surrounded mountain", "[ADVENTURER]") {
      std::string moves = "ADADADADA";
      Map<Object> map(6, 6, Object(Object::Type::Lowland, '.'));
      Adventurer lara("Lara", 2, 2, Orientation::South, moves);
      Object mountain(Object::Mountain, 'M');
      map[3][2] = mountain;
      map[2][3] = mountain;
      map[1][2] = mountain;
      map[2][1] = mountain;
      for (std::size_t i = 0; i < moves.size(); i++) {
            lara.move(map);
      }
      REQUIRE(lara.getXCoordinate() == 2);
      REQUIRE(lara.getYCoordinate() == 2);  
}

Map<Object> map(6, 6, Object(Object::Type::Lowland, '.'));
Adventurer lara("Lara", 2, 2, Orientation::South, "AADAAGA");

SCENARIO("Adventurer moves AADAAGA", "[ADVENTURER]") {
      GIVEN("A 6x6 map with adventurer in (2,2) / treasure in (1,4)") {
            Object treasure(Object::Type::Treasure, 'T', 2);
            map[4][1] = treasure;
            WHEN("1st move : A") {
                  lara.move(map);
                  REQUIRE(lara.getXCoordinate() == 2);
                  REQUIRE(lara.getYCoordinate() == 3);
            }
            WHEN("2nd move : A") {
                  lara.move(map);
                  REQUIRE(lara.getXCoordinate() == 2);
                  REQUIRE(lara.getYCoordinate() == 4);
            }
            WHEN("3rd and 4th moves : DA") {
                  lara.move(map);
                  lara.move(map);
                  REQUIRE(lara.getXCoordinate() == 1);
                  REQUIRE(lara.getYCoordinate() == 4);
            }
            WHEN("5th move : A") {
                  lara.move(map);
                  REQUIRE(lara.getXCoordinate() == 0);
                  REQUIRE(lara.getYCoordinate() == 4);
            }
            WHEN("6th and 7th moves : GA") {
                  lara.move(map);
                  lara.move(map);
                  REQUIRE(lara.getXCoordinate() == 0);
                  REQUIRE(lara.getYCoordinate() == 5);
                  REQUIRE(lara.getNumberOfTreasures() == 1);
            }
      }
}