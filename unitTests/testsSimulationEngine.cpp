#include "SimulationEngine.hh"
#include "Catch.hpp"

TEST_CASE("Simulation with no existing map", "[SIMULATION ENGINE]") {
      REQUIRE_THROWS_AS(SimulationEngine::simulate("../../maps/toto.txt"), std::runtime_error);
}

TEST_CASE("Simulation with map with no writing right", "[SIMULATION ENGINE]") {
      REQUIRE_THROWS_AS(SimulationEngine::simulate("../../maps/mapWithNoLectureRight.txt"), std::runtime_error);
}

TEST_CASE("Simulation with map1", "[SIMULATION ENGINE]") {
      std::string result = "C - 6 - 4\nT - 3 - 0 - 2\nM - 5 - 0\n";
      result += "M - 1 - 1\nM - 2 - 3\nA - Lara - 0 - 3 - S - 0\n";
      std::string resultGot{};

      resultGot = SimulationEngine::simulate("../../maps/map1.txt");
      REQUIRE(result == resultGot);
}

TEST_CASE("Simulation with map2", "[SIMULATION ENGINE]") {
      std::string result = "C - 3 - 4\nM - 1 - 0\nM - 2 - 1\n";
      result += "T - 1 - 3 - 2\nA - Lara - 0 - 3 - S - 3\n";
      std::string resultGot{};

      resultGot = SimulationEngine::simulate("../../maps/map2.txt");
      REQUIRE(result == resultGot);
}

TEST_CASE("Simulation with map3", "[SIMULATION ENGINE]") {
      std::string result = "C - 2 - 2\nM - 1 - 0\nT - 0 - 1 - 2\n";
      result += "M - 1 - 1\nA - Bob - 0 - 1 - S - 2\n";
      std::string resultGot{};

      resultGot = SimulationEngine::simulate("../../maps/map3.txt");
      REQUIRE(result == resultGot);
}

TEST_CASE("Simulation with map4", "[SIMULATION ENGINE]") {
      std::string result = "C - 6 - 3\nM - 0 - 0\nM - 2 - 0\n";
      result += "M - 3 - 0\nM - 5 - 0\nM - 0 - 1\nM - 5 - 1\n";
      result += "M - 0 - 2\nM - 2 - 2\nM - 3 - 2\nM - 5 - 2\n";
      result += "A - Lara - 1 - 1 - O - 2\n";
      result += "A - Bob - 1 - 2 - O - 0\n";
      result += "A - Tom - 4 - 1 - E - 2\n";
      result += "A - Bruce - 4 - 2 - E - 0\n";
      std::string resultGot{};

      resultGot = SimulationEngine::simulate("../../maps/map4.txt");
      REQUIRE(result == resultGot);
}

TEST_CASE("Simulation with map5", "[SIMULATION ENGINE]") {
      std::string result = "C - 10 - 5\nM - 0 - 0\nM - 1 - 0\n";
      result += "M - 2 - 0\nM - 3 - 0\nM - 4 - 0\nM - 5 - 0\n";
      result += "M - 6 - 0\nM - 7 - 0\nM - 0 - 1\nM - 1 - 1\n";
      result += "M - 2 - 1\nM - 3 - 1\nM - 0 - 2\nM - 5 - 2\n";
      result += "M - 6 - 2\nM - 7 - 2\nM - 0 - 3\nM - 1 - 3\n";
      result += "M - 2 - 3\nM - 3 - 3\nM - 0 - 4\nM - 1 - 4\n";
      result += "M - 2 - 4\nM - 3 - 4\nM - 4 - 4\nM - 5 - 4\n";
      result += "M - 6 - 4\nM - 7 - 4\nA - Lara - 1 - 2 - O - 3\n";
      result += "A - Bob - 2 - 2 - O - 0\n";
      std::string resultGot{};

      resultGot = SimulationEngine::simulate("../../maps/map5.txt");
      REQUIRE(result == resultGot);
}