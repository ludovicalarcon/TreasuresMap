#include "Object.hh"

Object::Object(Type const objType, char const objAbbreviation, std::size_t const objCount) :
      type{objType},
      abbreviation{objAbbreviation},
      count{objCount}
{
      if (objType == Object::Type::Mountain) {
            this->isOccupied = true;
      }
}

Object::Object(const Object &other) {
      this->type = other.type;
      this->abbreviation = other.abbreviation;
      this->count = other.count;
      this->isOccupied = other.isOccupied;
}