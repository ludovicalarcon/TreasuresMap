#include "Adventurer.hh"

#include <iostream>

Adventurer::Adventurer(std::string const &advName, std::size_t advX,
                  std::size_t advY, Orientation const advOrientation,
                  std::string const &moves) : 
                  name{advName},
                  coordX{advX},
                  coordY{advY},
                  currentOrientation{advOrientation},
                  numberOfTreasures{0},
                  movements {},
                  currentMovement {}
{
      for (std::size_t i = 0; i < moves.size(); i++) {
            this->movements.push_back(getMovementToken(moves[i]));
      }
}

Adventurer::Adventurer(Adventurer const &other) {
      this->name = other.name;
      this->coordX = other.coordX;
      this->coordY = other.coordY;
      this->currentOrientation = other.currentOrientation;
      this->numberOfTreasures = other.numberOfTreasures;
      this->movements = other.movements;
      this->currentMovement = other.currentMovement;
}

std::string const &Adventurer::getName() const {
      return this->name;
}

std::size_t Adventurer::getXCoordinate() const {
      return this->coordX;
}

std::size_t Adventurer::getYCoordinate() const {
      return this->coordY;
}

std::size_t Adventurer::getNumberOfMovements() const {
      return this->movements.size();
}

std::size_t Adventurer::getNumberOfTreasures() const {
      return this->numberOfTreasures;
}

char  Adventurer::getOrientationAbbreviation() const {
      switch (this->currentOrientation) {
            case Orientation::North:
                  return 'N';
            case Orientation::East:
                  return 'E';
            case Orientation::South:
                  return 'S';
            case Orientation::West:
                  return 'O';
            default:
                  return '\0';
      }
}

Movement    Adventurer::getMovementToken(char const abbreviation) {
      switch (abbreviation) {
            case 'A':
                  return Movement::Forward;
            case 'D':
                  return Movement::Right;
            case 'G':
                  return Movement::Left;
            default:
                  return Movement::Unknown;
      }
}

void  Adventurer::move(Map<Object> &map) {
      if (this->currentMovement >= this->movements.size()) {
            return ;
      }
      switch (this->movements[this->currentMovement]) {
            case Movement::Forward:
                  moveForward(map);
            break;
            case Movement::Left:
                  moveLeft();
            break;
            case Movement::Right:
                  moveRight();
            break;
            default:
            break;
      }
      ++this->currentMovement;
}

void Adventurer::moveRight() {
      int newOrientation = this->currentOrientation;
      ++newOrientation;
      if (newOrientation >= Orientation::Undefined) {
            newOrientation = 0;
      }
      this->currentOrientation = static_cast<Orientation>(newOrientation);
}

void Adventurer::moveLeft() {
      int newOrientation = this->currentOrientation;
      --newOrientation;
      if (newOrientation < 0) {
            newOrientation = Orientation::Undefined - 1;
      }
      this->currentOrientation = static_cast<Orientation>(newOrientation);
}

void Adventurer::moveForward(Map<Object> &map) {
           switch (this->currentOrientation) {
            case Orientation::North:
                  if (!moveToNorth(map)) {
                        return ;
                  }
            break;
            case Orientation::East:
                  if (!moveToEast(map)) {
                        return ;
                  }
            break;
            case Orientation::South:
                  if (!moveToSouth(map)) {
                        return ;
                  }
            break;
            case Orientation::West:
                  if (!moveToWest(map)) {
                        return ;
                  }
            break;
            case Orientation::Undefined:
            default:
            break;
      }   
      Object obj = map.getElement(this->coordX, this->coordY);
      if (obj.type == Object::Type::Treasure && obj.count > 0) {
            --map.getElement(this->coordX, this->coordY).count;
            ++this->numberOfTreasures;
      }
      map.getElement(this->coordX, this->coordY).isOccupied = true;
}

bool  Adventurer::moveToNorth(Map<Object> &map) {
      if (this->coordY == 0) {
            return false;
      }
      if (map.getElement(this->coordX, this->coordY - 1).isOccupied) {
            return false;
      }
      map.getElement(this->coordX, this->coordY).isOccupied = false;
      --this->coordY;
      return true;
}

bool  Adventurer::moveToEast(Map<Object> &map) {
      if (this->coordX >= map.getWidth() - 1) {
            return false;
      }
      if (map.getElement(this->coordX + 1, this->coordY).isOccupied) {
            return false;
      }
      map.getElement(this->coordX, this->coordY).isOccupied = false;
      ++this->coordX;
      return true;
}

bool  Adventurer::moveToSouth(Map<Object> &map) {
      if (this->coordY >= map.getHeight() - 1) {
            return false;
      }
      if (map.getElement(this->coordX, this->coordY + 1).isOccupied) {
            return false;
      }
      map.getElement(this->coordX, this->coordY).isOccupied = false;
      ++this->coordY;
      return true;
}

bool  Adventurer::moveToWest(Map<Object> &map) {
      if (this->coordX == 0) {
            return false;
      }
      if (map.getElement(this->coordX - 1, this->coordY).isOccupied) {
            return false;
      }
      map.getElement(this->coordX, this->coordY).isOccupied = false;
      --this->coordX;
      return true;
}