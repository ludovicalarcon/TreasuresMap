#include "SimulationEngine.hh"

int main(int ac, char **av)
{
      if (ac != 2) {
            std::cerr << "Usage: ./[BINARY] [MAP]" << std::endl;
            return 1;
      }
      try {
            SimulationEngine::simulate(std::string(av[1]));
      }
      catch (std::exception const &e) {
            std::cerr << e.what() << std::endl;
      }
      return 0;
}