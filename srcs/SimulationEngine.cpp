#include "SimulationEngine.hh"
#include "Parser.hpp"

#include <fstream>
#include <sstream>

SimulationEngine::SimulationEngine() {

}

std::string const SimulationEngine::simulate(std::string const &filePath) {
      SimulationEngine engine;

      engine.startSimulation(filePath);
      engine.logResultInFile("output_simulation.txt");
      return engine.getResult();
}

void SimulationEngine::startSimulation(std::string const &filePath) {
      Parser parser(filePath);

      this->map = parser.parseFromFile();
      this->adventurers = parser.getAdventurersList();
      simulationLoop();
}

void SimulationEngine::simulationLoop() {
      std::size_t iteration = getMaxIterationNeeded();

      while (iteration > 0) {
            for (auto it = this->adventurers.begin(); it != this->adventurers.end(); ++it) {
                  it->move(this->map);
            }
            --iteration;
      }

}

std::size_t SimulationEngine::getMaxIterationNeeded() const {
      std::size_t maxIteration = 0;
      std::size_t current = 0;

      for (auto it = this->adventurers.begin(); it != this->adventurers.end(); ++it) {
            current = it->getNumberOfMovements();
            if (current > maxIteration) {
                  maxIteration = current;
            }
      }
      return maxIteration;
}

void  SimulationEngine::logResultInFile(std::string const &filePath) const {
      std::ofstream logFile(filePath);
      std::string log{};

      if (!logFile.is_open()) {
            throw std::runtime_error(std::string("Error: File " + filePath + " cannot be opened"));
      }
      log = logResult();
      logFile << log;
}

std::string const SimulationEngine::logResult() const {
      std::stringstream log{};

      log << "C - " << this->map.getWidth() << " - " << this->map.getHeight() << std::endl;
      logObject(log);
      for (auto it = this->adventurers.begin(); it != this->adventurers.end(); it++) {
            log << "A - " << it->getName() << " - " << it->getXCoordinate();
            log << " - " << it->getYCoordinate() << " - " << it->getOrientationAbbreviation();
            log << " - " << it->getNumberOfTreasures() << std::endl;
      }
      return log.str();
}

void  SimulationEngine::logObject(std::stringstream &log) const {
      for (std::size_t y = 0; y < this->map.getHeight(); y++) {
            for (std::size_t x = 0; x < this->map.getWidth(); x++) {
                  Object obj = this->map.getElement(x, y);
                  if (obj.type != Object::Lowland && obj.type != Object::Adventurer
                        && obj.count > 0) {
                        log << obj.abbreviation << " - " << x << " - " << y;
                        if (obj.type == Object::Treasure) {
                              log << " - " << obj.count;
                        }
                        log << std::endl;
                  }
            }
      }

}

std::string const SimulationEngine::getResult() const {
      return logResult();
}