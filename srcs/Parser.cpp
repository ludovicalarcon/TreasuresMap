#include "Parser.hpp"

#include <algorithm>

Parser::Parser(std::string const &filePath) : 
                  file{filePath},
                  parsingMethods{&Parser::addMountainOnMap,
                              &Parser::addTreasureOnMap,
                              &Parser::addAdventurerOnMap,
                              &Parser::resizeMapWithFinalSize,
                              &Parser::addComment,
                              nullptr,
                              &Parser::unknownCommand}
{
      if (!this->file.is_open())
            throw std::runtime_error(std::string("Error: File " + filePath + " cannot be opened"));
}

Map<Object> const   &Parser::parseFromFile() {
      return computeMap();
}

std::vector<Adventurer> const &Parser::getAdventurersList() const {
      return this->adventurers;
}

Map<Object> const   &Parser::computeMap() {
      std::string line {};
      Object::Type tokenType = Object::Type::Undefined;

      while (std::getline(this->file, line))
      {
            line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
            tokenType = getTokenType(line[0]);
            if ((line.size() <= 2 || line[1] != '-') && tokenType != Object::Type::Comment) {
                  tokenType = Object::Type::Undefined;
            }
            if (!(this->*parsingMethods[tokenType])(line)) {
                  std::cerr << "[Warning] line " << this->lineNumber << " : "
                  << "Wrong syntax or wrong coordinates, line will be skiped" << std::endl;
            }
            ++this->lineNumber;
      }
      if (!this->foundMapSize) {
            throw std::runtime_error("[Error] File doesn't define map size");
      }
      return this->map;
}

Object::Type  Parser::getTokenType(char const abbreviation) const {
      switch (abbreviation) {
            case 'M':
                  return Object::Type::Mountain;
            case 'T':
                  return Object::Type::Treasure;
            case 'A':
                  return Object::Type::Adventurer;
            case 'C':
                  return Object::Type::Map;
            case '#':
                  return Object::Type::Comment;
            default:
                  return Object::Type::Undefined;
      }
}

Orientation Parser::getOrientationByToken(char const token) const {
      switch (token) {
            case 'N':
                  return Orientation::North;
            case 'S':
                  return Orientation::South;
            case 'E':
                  return Orientation::East;
            case 'O':
                  return Orientation::West;
            default:
                  return Orientation::Undefined;
      }
}

bool Parser::checkForResizingMap(std::size_t x, std::size_t y, Object const &obj) {
      if (!this->foundMapSize) {
            if (x < this->mapXCoord && y >= this->mapYCoord) {
                  this->map.resizeTo(this->mapXCoord, y, obj);
            }
            else if (x >= this->mapXCoord && y < this->mapYCoord) {
                  this->map.resizeTo(x, this->mapYCoord, obj);   
            }
            else if (x >= this->mapXCoord && y >= this->mapYCoord) {
                  this->map.resizeTo(x, y, obj);
            }
      }
      else {
            if (x > this->mapXCoord && y > this->mapYCoord) {
                  return false;
            }
      }
      return true;
}

bool  Parser::updateInternalFields(Object const &obj, std::size_t x, std::size_t y, bool isMountain) {
      this->mapXCoord = this->map.getWidth();
      this->mapYCoord = this->map.getHeight();
      if (this->map[y][x].type == Object::Type::Mountain && !isMountain) {
            return false;
      }
      this->map[y][x] = obj;
      return true;
}

bool  Parser::addMountainOnMap(std::string const &line) {
      std::size_t x {}, y {};
      std::size_t foundPosition {};

      foundPosition = line.find("-", 2);
      if (foundPosition == std::string::npos) {
            return false;
      }
      x = extractDataFromString<std::size_t>(line, 2, foundPosition - 2);
      y = extractDataFromString<std::size_t>(line, foundPosition + 1, line.size() - foundPosition);
      Object const obj(Object::Type::Lowland, '.');
      Object const mountain(Object::Type::Mountain, 'M');
      if (!checkForResizingMap(x, y, obj)) {
            return false;
      }
      updateInternalFields(mountain, x, y, true);
      return true;
}

bool  Parser::addTreasureOnMap(std::string const &line) {
      std::size_t x {}, y {}, number {};
      std::size_t foundPosition {}, tmpFoundPosition {};

      foundPosition = line.find("-", 2);
      if (foundPosition == std::string::npos) {
            return false;
      }
      x = extractDataFromString<std::size_t>(line, 2, foundPosition - 2);
      tmpFoundPosition = foundPosition;
      foundPosition = line.find("-", tmpFoundPosition + 1);
      if (foundPosition == std::string::npos) {
            return false;
      }
      y = extractDataFromString<std::size_t>(line, tmpFoundPosition + 1, foundPosition - tmpFoundPosition);
      number = extractDataFromString<std::size_t>(line, foundPosition + 1, line.size() - foundPosition);
      Object const obj(Object::Type::Lowland, '.');
      Object const treasure(Object::Type::Treasure, 'T', number);
      if (!checkForResizingMap(x, y, obj)) {
            return false;
      }
      return updateInternalFields(treasure, x , y);
}

bool  Parser::addAdventurerOnMap(std::string const &line) {
      std::size_t x {}, y {};
      std::size_t foundPosition {}, tmpFoundPosition {};
      std::string name {}, movements {};
      Orientation orientation = Orientation::Undefined;

      foundPosition = line.find("-", 2);
      if (foundPosition == std::string::npos) {
            return false;
      }
      name = extractDataFromString<std::string>(line, 2, foundPosition - 2);
      tmpFoundPosition = foundPosition;
      foundPosition = line.find("-", tmpFoundPosition + 1);
      if (foundPosition == std::string::npos) {
            return false;
      }
      x = extractDataFromString<std::size_t>(line, tmpFoundPosition + 1, foundPosition - tmpFoundPosition);
      tmpFoundPosition = foundPosition;
      foundPosition = line.find("-", tmpFoundPosition + 1);
      if (foundPosition == std::string::npos) {
            return false;
      }
      y = extractDataFromString<std::size_t>(line, tmpFoundPosition + 1, foundPosition - tmpFoundPosition);
      orientation = getOrientationByToken(line[foundPosition + 1]);
      foundPosition = line.find("-", foundPosition + 1);
      if (foundPosition == std::string::npos) {
            return false;
      }
      movements = extractDataFromString<std::string>(line, foundPosition + 1, line.size() - foundPosition);
      Object const obj(Object::Type::Lowland, '.');
      Object const adventurerObj(Object::Type::Adventurer, 'A');
      if (!checkForResizingMap(x, y, obj) || !updateInternalFields(adventurerObj, x, y)) {
            return false;
      }
      Adventurer adventurer(name, x, y, orientation, movements);
      this->adventurers.push_back(adventurer);
      return true;
}

bool  Parser::resizeMapWithFinalSize(std::string const &line) {
      std::size_t x {}, y {};
      std::size_t foundPosition {};

      foundPosition = line.find("-", 2);
      if (foundPosition == std::string::npos) {
            return false;
      }
      x = extractDataFromString<std::size_t>(line, 2, foundPosition - 2);
      y = extractDataFromString<std::size_t>(line, foundPosition + 1, line.size() - foundPosition);
      Object obj(Object::Type::Lowland, '.');
      this->map.resizeTo(x, y, obj, true);
      this->mapXCoord = this->map.getWidth();
      this->mapYCoord = this->map.getHeight();
      this->foundMapSize = true;
      return true;
}

bool  Parser::addComment(std::string const &) {
      // Do nothing to skip comments
      return true;
}

bool Parser::unknownCommand(std::string const &) {
      std::cerr << "[Warning] line " << this->lineNumber << " : "
      << "Unknown command, line will be skiped" << std::endl;
      return true;
}
